﻿using System;
using System.Runtime.InteropServices;

namespace GamblingTester
{
    public class Program
    {
        // These two should be tied together technically and affect the win amount received based on the roll under to win number - change these two vars together
        private const double BetMultiplier = 1.25;
        private const double RollUnderToWinNumber = 80;

        // LOSS CHOICES
        // Change this to select your on loss bet choice
        private static readonly BetChoices OnLossBetChoice = BetChoices.INCREASE_BET;
        
        // Change this percentage amount to increase or decrease bet on loss - OnLossBetChoice must be set to Increase_Bet or Decrease_Bet for this to be enabled.
        private const double LossIncreaseOrDecreaseMultiplierPercentage = 400; // 0 for no increase, 100 to double bet, 200 to triple, etc
        
        // Set this above 0 to be enabled, determines if bet amount should be reset after a certain amount of losses.
        private const int MaxLossesBeforeResettingBet = 0;
        
        // WIN CHOICES
        // Change this to select your on win bet choice
        private static readonly BetChoices OnWinBetChoice = BetChoices.RESET_TO_MIN;

        // Change this percentage amount to increase or decrease bet on win - OnWinBetChoice must be set to Increase_Bet or Decrease_Bet for this to be enabled.
        private const double WinIncreaseOrDecreaseMultiplierPercentage = 0; // 0 for no increase, 100 to double bet, 200 to triple, etc
        
        // Set this above 0 to be enabled, determines if bet amount should be reset after a certain amount of wins.
        private const int MaxWinsBeforeResettingBet = 0;
        
        // Enable this flag and set the win goal amount if the program should stop at a certain bank amount obtained
        private const bool EnableWinGoal = false;
        private const double WinGoal = 35;
        
        private const double MinimumBet = 0.1;
        
        // Starting bank amount - set to whatever u like
        private static double bankAmount = 25;
        
        // Change this to set starting bet amount
        private static double betAmount = MinimumBet;
        
        // Variables for tracking data - no need to change these vars.
        private static double maxBankAccountSeen = 0;
        private static int gameNumberForMaxBankAccountSeen = 0;
        
        public static void Main(string[] args)
        {
            var randomGenerator = new Random();
            bool? didWin = null;
            var gameNumber = 1;

            var maxTotalWinsInARow = 0;
            var maxTotalLossesInARow = 0;
            var totalWins = 0;
            var totalLosses = 0;
            
            // NOTE: These counters will get reset on win/lose streaks, they should not be used for comparing max win/losses seen
            var lossesInARowCounter = 0;
            var winsInARowCounter = 0;
            
            while (bankAmount >= MinimumBet && betAmount >= MinimumBet)
            {
                // record max bank account seen
                if (bankAmount > maxBankAccountSeen)
                {
                    maxBankAccountSeen = bankAmount;
                    gameNumberForMaxBankAccountSeen = gameNumber;
                }
                
                // If hit win goal, exit
                if (EnableWinGoal && bankAmount >= WinGoal)
                {
                    Console.WriteLine($"Hit win goal of {WinGoal}, exiting program. Bank amount: {bankAmount}, on game number: {gameNumber}");
                    return;
                }

                // Reset bets on user configured win or lose streaks
                var betReset = false;
                if ((MaxLossesBeforeResettingBet > 0 && lossesInARowCounter == MaxLossesBeforeResettingBet) || (MaxWinsBeforeResettingBet > 0 && winsInARowCounter == MaxWinsBeforeResettingBet))
                {
                    betAmount = MinimumBet;
                    betReset = true;
                    
                    // Incase there is a long win streak, reset it again, so that next win streak will reset min bet
                    if (MaxWinsBeforeResettingBet > 0 && winsInARowCounter == MaxWinsBeforeResettingBet)
                    {
                        winsInARowCounter = 0;
                    }
                    // Incase there is a long lose streak, reset it again, so that next loss streak will reset min bet
                    else if (MaxLossesBeforeResettingBet > 0 && lossesInARowCounter == MaxLossesBeforeResettingBet)
                    {
                        lossesInARowCounter = 0;
                    }
                }
                
                // Handle bet increases/decreases on each win or loss
                if (didWin.HasValue && didWin.Value && !betReset)
                {
                    switch (OnWinBetChoice)
                    {
                        case BetChoices.INCREASE_BET:
                            betAmount += betAmount * (WinIncreaseOrDecreaseMultiplierPercentage / 100);
                            break;
                        case BetChoices.DECREASE_BET:
                            betAmount -= betAmount * (WinIncreaseOrDecreaseMultiplierPercentage / 100);
                            break;
                        case BetChoices.RESET_TO_MIN:
                            betAmount = MinimumBet;
                            break;
                        case BetChoices.SAME_BET:
                        default:
                            // Do nothing to bet amount
                            break;
                    }
                }
                else if (didWin.HasValue && !didWin.Value && !betReset)
                {
                    switch (OnLossBetChoice)
                    {
                        case BetChoices.INCREASE_BET:
                            betAmount += betAmount * (LossIncreaseOrDecreaseMultiplierPercentage / 100);
                            break;
                        case BetChoices.DECREASE_BET:
                            betAmount -= betAmount * (LossIncreaseOrDecreaseMultiplierPercentage / 100);
                            break;
                        case BetChoices.RESET_TO_MIN:
                            betAmount = MinimumBet;
                            break;
                        case BetChoices.SAME_BET:
                        default:
                            // Do nothing to bet amount
                            break;
                    }
                }

                // If cant bet anymore, exit
                if (betAmount > bankAmount)
                {
                    Console.WriteLine($"Not enough to bet using multiplier, exiting program. Bank amount: {bankAmount}, bet amount: {betAmount}" +
                                      $"\n Max bank account seen: {maxBankAccountSeen} on game number: {gameNumberForMaxBankAccountSeen}" +
                                      $"\n Total Wins: {totalWins}, Total Losses: {totalLosses}, Longest Win Streak: {maxTotalWinsInARow}, Longest Loss Streak: {maxTotalLossesInARow}");
                    return;
                }
                
                Console.WriteLine($"Game #: {gameNumber}, Bank Amount: {bankAmount}, bet amount: {betAmount}");
                bankAmount -= betAmount;
                
                var outcome = randomGenerator.Next(1, 101); // 1 - 100
                
                // Check if game was won
                if (outcome < RollUnderToWinNumber)
                {
                    // Won game
                    lossesInARowCounter = 0;
                    totalWins++;
                    
                    // If last roll was a win and we win again, increment win counter
                    if (didWin.HasValue && didWin.Value == true)
                    {
                        winsInARowCounter = winsInARowCounter == 0 ? 2 : winsInARowCounter + 1;
                        if (maxTotalWinsInARow < winsInARowCounter)
                        {
                            maxTotalWinsInARow = winsInARowCounter;
                        }
                    }
                    
                    didWin = true;
                    
                    // Award win amount to bank account
                    var winAmount = betAmount * BetMultiplier;
                    bankAmount += winAmount;
                    
                    Console.WriteLine($"Rolled: {outcome}, WON {winAmount}, bank amount: {bankAmount}");
                }
                else
                {
                    // Lost Game - Note: bet is deducted before game begins, no need to do anything here.
                    winsInARowCounter = 0;
                    totalLosses++;
                    
                    // If last roll was a loss and we lost again, increment loss counter
                    if (didWin.HasValue && didWin.Value == false)
                    {
                        lossesInARowCounter = lossesInARowCounter == 0 ? 2 : lossesInARowCounter + 1;
                        if (maxTotalLossesInARow < lossesInARowCounter)
                        {
                            maxTotalLossesInARow = lossesInARowCounter;
                        }
                    }
                    
                    didWin = false;
                    
                    Console.WriteLine($"Rolled: {outcome}, LOST, bank amount: {bankAmount}");
                }

                gameNumber++;
            }
            
            Console.WriteLine($"Bank amount {bankAmount} is not greater than min bet: {MinimumBet}, exiting program. " +
                              $"\n Max bank account seen: {maxBankAccountSeen} on game number: {gameNumberForMaxBankAccountSeen}" +
                              $"\n Total Wins: {totalWins}, Total Losses: {totalLosses}, Longest Win Streak: {maxTotalWinsInARow}, Longest Loss Streak: {maxTotalLossesInARow}");
        }
    }
}