namespace GamblingTester
{
    public enum BetChoices
    {
        SAME_BET = 0,
        
        INCREASE_BET = 1,
        
        DECREASE_BET = 2,
        
        RESET_TO_MIN = 3
    }
}